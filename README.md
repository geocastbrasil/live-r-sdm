# Live R - SDM

Repositório para discussão e preparação das lives de R sobre Ecological Niche Modeling / Species Distribution Modeling

### Algumas propostas

Modelagem / aprendizado supervisionado/ limpeza e organização dos dados / modelos de nicho

Data da próxima conversa: **03/11**, 13 h)  
Data proposta de início: **Antes e depois carnaval**)

1. Live 1: Modelos estatísticos
    1.1. introdução ao tema e aplicações  
    1.2. introdução aos modelos estatísticos:  
        1.2.1. lm  
        1.2.2. glm  
        
1. Live 2: Aprendizado supervisionado  
    2.1. tipos de algoritmos (Classificlação/Regressão)  
    2.2. crossvalidation/bootstrap  
    2.2. matriz de confusão  

1. Live 3: Nicho ecológico, distribuição geográfica e operacionalização  
    3.1. teoria de nicho ecológico  
    3.2. operacionalização  
    3.3. dados: ocorrências - bases de dados, download e limpeza  
    3.4. dados: variáveis - bases de dados, download, ajustes e colinearidade  
    3.5. análise exploratória de dados

1. Live 4: Modelo de Nicho Ecológico (MNE)  
    4.1. entrada dos dados  
    4.2. ajuste dos modelos  
    4.3. predição dos modelos  
    4.4. avaliação dos modelos
    4.5. outras perspectivas

